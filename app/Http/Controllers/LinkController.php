<?php

namespace App\Http\Controllers;

use App\Models\Link;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class LinkController extends Controller
{
	public function make(Request $request)
	{

		$validator = Validator::make($request->all(), [
			'url' => 'required|url|max:255'
		]);

		if ($validator->fails()) {
			return Redirect::route('home')->withInput()->withErrors($validator);
		}

		$messages = [
			'success' => 'All done! Here is your short URL <a href="%1$s">%1$s</a>',
			'error'   => 'Something went wrong. Try again!'
		];

		$url  = $request->get('url');
		$code = Link::isLinkExist($url);

		if (empty($code)) {
			$code = base_convert(microtime(true), 10, 36);
			$row  = Link::create([
				'url'  => $url,
				'code' => $code
			]);

			if (!$row->exists) {
				return Redirect::action('HomeController@index')->with('message', $messages['error']);
			}
		}

		return Redirect::route('home')->with('message', sprintf($messages['success'], route('link.get', ['code' => $code])));
	}

	public function get($code)
	{
		$link = Link::where('code', '=', $code)->first();

		if (empty($link)) {
			return Redirect::route('home');
		}

		return Redirect::to($link->url);
	}
}
