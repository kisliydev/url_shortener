<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url', 'code'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * Check if Link already exist in table. If exist return code column.
     *
     * @param string $url
     * @return bool | string
     */
    static function isLinkExist($url)
    {
        if(empty($url)) {
            return false;
        }

        $row = self::where('url', '=', $url)->first();

        if(empty($row)) {
            return false;
        }

        return $row->code;
    }
}
