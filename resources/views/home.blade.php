<!doctype html>
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
        <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <title>{{ config('app.name') }}</title>

            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

            <link rel="stylesheet" href="{{ URL::to('css/global.css') }}">
        </head>
    <body>
        <main role="main" class="container h-100">
            <div class="row h-100 justify-content-center align-items-center">
                <div class="col-lg-6 col-12">
                    <h1 class="title">{{ config('app.name') }}</h1>

                    <blockquote class="blockquote text-center">
                        <p class="mb-0">The power is in our hands to make the url a better.</p>
                        <footer class="blockquote-footer">Someone famous in <cite title="Source Title">WEB</cite></footer>
                    </blockquote>

                    @if($errors->has('url'))
                        <div class="alert alert-danger" role="alert">
                            {{ $errors->first('url') }}
                        </div>
                    @endif

                    @if(Session::has('message'))
                        <div class="alert alert-success" role="alert">
                            {!! Session::get('message') !!}
                        </div>
                    @endif

                    {{ Form::open(['route' => 'link.make']) }}

                        <div class="form-group">
                            {{ Form::url('url', Input::old('url') ?? '', [
                                    'placeholder' => 'Enter a URL',
                                    'required' => 'true',
                                    'class' => 'form-control'
                            ]) }}
                        </div>

                        {{ Form::button('Shorten', [
                            'class' => 'btn btn-primary',
                            'type' => 'submit'
                        ]) }}

                    {{ Form::close() }}
                </div>
            </div>
        </main>
        <footer class="footer">
            <div class="container">
                <span>By Kisliy Maxim</span>
            </div>
        </footer>
    </body>
</html>
